package me.Ghostrider.SuperLuckyBlock;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.SkullMeta;

public class SLBCommandExecutor
  implements CommandExecutor
{
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
  {
    if (cmd.getName().equalsIgnoreCase("slb"))
    if (sender.hasPermission("SuperLuckyBlock.give"))
  {
    Player player = (Player)sender;
    PlayerInventory inventory = player.getInventory();
    ItemStack lb = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
    SkullMeta meta = (SkullMeta)Bukkit.getItemFactory().getItemMeta(Material.SKULL_ITEM);
    
    meta.setDisplayName(ChatColor.GOLD + "Lucky Block");
    meta.setOwner("Luck");
    lb.setItemMeta(meta);
    inventory.addItem(new ItemStack(lb));
    sender.sendMessage(ChatColor.YELLOW + "[SLB]" + ChatColor.WHITE + "Gave you a LuckyBlock!");
    return true;
  }
return false;
}
}