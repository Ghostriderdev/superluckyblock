package me.Ghostrider.SuperLuckyBlock;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Skull;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SuperLuckyBlock extends JavaPlugin implements Listener
{
  FileConfiguration config = getConfig();
  public Cooldown cooldown;
  HashMap<String, Integer> cooldown1;
  public SuperLuckyBlock plugin;
  
  public void onDisable() {}
  
  public void onEnable()
  {
    Bukkit.getPluginManager().registerEvents(this, this);
    ConfigDefaults();
    saveConfig();
    Crafting();
    FastBreak();
    getCommand("slb").setExecutor(new SLBCommandExecutor());
  }
  
  private void FastBreak()
  {
    if (this.config.getBoolean("settings.FastBreak"))
    {
      String a = getServer().getClass().getPackage().getName();
      String version = a.substring(a.lastIndexOf('.') + 1);
      if (version.equalsIgnoreCase("v1_9_R2"))
      {
        try
        {
          Field field = net.minecraft.server.v1_9_R2.Block.class.getDeclaredField("strength");
          field.setAccessible(true);
          field.setFloat(net.minecraft.server.v1_9_R2.Blocks.SKULL, Material.SKULL.getMaxDurability() / 4);
        }
        catch (Exception ex)
        {
          ex.printStackTrace();
        }
        if (version.equalsIgnoreCase("v1_9_R1"))
        {
          try
          {
            Field field = net.minecraft.server.v1_9_R1.Block.class.getDeclaredField("strength");
            field.setAccessible(true);
            field.setFloat(net.minecraft.server.v1_9_R1.Blocks.SKULL, Material.SKULL.getMaxDurability() / 4);
          }
          catch (Exception ex)
          {
            ex.printStackTrace();
          }
          if (version.equalsIgnoreCase("v1_10_R1")) {
            try
            {
              Field field = net.minecraft.server.v1_10_R1.Block.class.getDeclaredField("strength");
              field.setAccessible(true);
              field.setFloat(net.minecraft.server.v1_10_R1.Blocks.SKULL, Material.SKULL.getMaxDurability() / 4);
            }
            catch (Exception ex)
            {
              ex.printStackTrace();
            }
          }
        }
      }
    }
  }
  
  private void Crafting()
  {
    ItemStack lb = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
    SkullMeta meta = (SkullMeta)Bukkit.getItemFactory().getItemMeta(Material.SKULL_ITEM);
    meta.setDisplayName(ChatColor.GOLD + "Lucky Block");
    meta.setOwner("Luck");
    lb.setItemMeta(meta);
    if (this.config.getBoolean("settings.CraftingRecipe")) {}
    lb.setItemMeta(meta);
    ShapedRecipe luckyblock = new ShapedRecipe(lb);
    luckyblock.shape(new String[] { "***", "*D*", "***" });
    luckyblock.setIngredient('*', Material.GOLD_INGOT);
    luckyblock.setIngredient('D', Material.DROPPER);
    getServer().addRecipe(luckyblock);
  }
  
@SuppressWarnings("deprecation")
@EventHandler
  public void breakListener(BlockBreakEvent event)
  {
    Player p = event.getPlayer();
    if (event.getBlock().getType() == Material.SKULL)
    //if (plugin.cooldown1.get(p.getName()) <= 0) {
    {
      //plugin.cooldown1.put(p.getName(), config.getInt("settings.CooldownTime"));
      Skull luckyblock = (Skull)event.getBlock().getState();
      if (luckyblock.getOwner().equalsIgnoreCase("Luck")){
        event.setCancelled(true);
        event.getPlayer().getWorld().getBlockAt(event.getBlock().getLocation()).setType(Material.AIR);
        
        if (this.config.getBoolean("settings.BroadcastBlockBreak")) {
          Bukkit.broadcastMessage(ChatColor.GOLD + "[SLB] " + event.getPlayer().getDisplayName() + "Has broken a Lucky Block!");
        }
        int howManyPossibleHappenstances = 53;
        
        int randomChoice = 1 + (int)(Math.random() * howManyPossibleHappenstances);
        
        Location l = event.getBlock().getState().getLocation();
        switch (randomChoice)
        {
        case 1: 
          if (this.config.getBoolean("surprises.enabled.LuckyTools")) {
            luckyTools(1, l, p);
          }
          break;
        case 2: 
          if (this.config.getBoolean("surprises.enabled.LuckyTools")) {
            luckyTools(2, l, p);
          }
          break;
        case 3: 
          if (this.config.getBoolean("surprises.enabled.LuckyTools")) {
            luckyTools(3, l, p);
          }
          break;
        case 4: 
          if (this.config.getBoolean("surprises.enabled.LuckyTools")) {
            luckyTools(4, l, p);
          }
          break;
        case 5: 
          if (this.config.getBoolean("surprises.enabled.LuckyTools")) {
            luckyTools(5, l, p);
          }
          break;
        case 6: 
          if (this.config.getBoolean("surprises.enabled.LuckyTools")) {
            luckyTools(6, l, p);
          }
          break;
        case 7: 
          if (this.config.getBoolean("surprises.enabled.enderPearls")) {
            enderPearls(l, p);
          }
          break;
        case 8: 
          if (this.config.getBoolean("surprises.enabled.explosion")) {
            explosion2(l, p);
          }
          break;
        case 9: 
          if (this.config.getBoolean("surprises.enabled.explosion")) {
            explosion3(l, p);
          }
          break;
        case 10: 
          if (this.config.getBoolean("surprises.enabled.explosion")) {
            explosion(l, p);
          }
          break;
        case 11: 
          if (this.config.getBoolean("surprises.enabled.wither")) {
            wither(l, p);
          }
          break;
        case 12: 
          if (this.config.getBoolean("surprises.enabled.woodTools")) {
            woodTools(l, p);
          }
          break;
        case 13: 
          if (this.config.getBoolean("surprises.enabled.witch")) {
            witch(l, p);
          }
          break;
        case 14: 
          if (this.config.getBoolean("surprises.enabled.stoneTools")) {
            stoneTools(l, p);
          }
          break;
        case 15: 
          if (this.config.getBoolean("surprises.enabled.Zombies")) {
            Zombie(l, p);
          }
          break;
        case 16: 
          if (this.config.getBoolean("surprises.enabled.goldTools")) {
            goldTools(l, p);
          }
          break;
        case 17: 
          if (this.config.getBoolean("surprises.enabled.lightningCreeper")) {
            lightningcreeper(l, p);
          }
          break;
        case 18: 
          if (this.config.getBoolean("surprises.enabled.leatherArmor")) {
            leatherArmor(l, p);
          }
          break;
        case 19: 
          if (this.config.getBoolean("surprises.enabled.ghast")) {
            ghast(l, p);
          }
          break;
        case 20: 
          if (this.config.getBoolean("surprises.enabled.goldArmor")) {
            goldArmor(l, p);
          }
          break;
        case 21: 
          if (this.config.getBoolean("surprises.enabled.creepers")) {
            creepers(l, p);
          }
          break;
        case 22: 
          if (this.config.getBoolean("surprises.enabled.gold")) {
            gold(l, p);
          }
          break;
        case 23: 
          if (this.config.getBoolean("surprises.enabled.ironTools")) {
            ironTools(l, p);
          }
          break;
        case 24: 
          if (this.config.getBoolean("surprises.enabled.diamondTools")) {
            diamondTools(l, p);
          }
          break;
        case 25: 
          if (this.config.getBoolean("surprises.enabled.beacon")) {
            beacon(l, p);
          }
          break;
        case 26: 
          if (this.config.getBoolean("surprises.enabled.ironArmor")) {
            ironArmor(l, p);
          }
          break;
        case 27: 
          if (this.config.getBoolean("surprises.enabled.enderChests")) {
            enderChests(l, p);
          }
          break;
        case 28: 
          if (this.config.getBoolean("surprises.enabled.diamondArmor")) {
            diamondArmor(l, p);
          }
          break;
        case 29: 
          if (this.config.getBoolean("surprises.enabled.dragonEgg")) {
            dragonEgg(l, p);
          }
          break;
        case 30: 
          if (this.config.getBoolean("surprises.enabled.enchantmentTable")) {
            enchantmentTable(l, p);
          }
          break;
        case 31: 
          if (this.config.getBoolean("surprises.enabled.horseArmor")) {
            horseArmor(l, p);
          }
          break;
        case 32: 
          if (this.config.getBoolean("surprises.enabled.obsidian")) {
            obsidian(l, p);
          }
          break;
        case 33: 
          if (this.config.getBoolean("surprises.enabled.TNT")) {
            TNT(l, p);
          }
          break;
        case 34: 
          if (this.config.getBoolean("surprises.enabled.bowAndArrows")) {
            bowAndArrows(l, p);
          }
          break;
        case 35: 
          if (this.config.getBoolean("surprises.enabled.brewingItems")) {
            brewingItems(l, p);
          }
          break;
        case 36: 
          if (this.config.getBoolean("surprises.enabled.Redstone")) {
            Redstone(l, p);
          }
          break;
        case 37: 
          if (this.config.getBoolean("surprises.enabled.booksAndBookshelves")) {
            booksAndBookshelves(l, p);
          }
          break;
        case 38: 
          if (this.config.getBoolean("surprises.enabled.meats")) {
            meats(l, p);
          }
          break;
        case 39: 
          if (this.config.getBoolean("surprises.enabled.vegetables")) {
            vegetables(l, p);
          }
          break;
        case 40: 
          if (this.config.getBoolean("surprises.enabled.PumpKins")) {
            PumpKins(l, p);
          }
          break;
        case 41: 
          if (this.config.getBoolean("surprises.enabled.JackOLanterns")) {
            JackOLanterns(l, p);
          }
          break;
        case 42: 
          if (this.config.getBoolean("surprises.enabled.Halloween1")) {
            halloween1(l, p);
          }
          break;
        case 43: 
          if (this.config.getBoolean("surprises.enabled.Halloween2")) {
            halloween2(l, p);
          }
          break;
        case 44: 
          if (this.config.getBoolean("surprises.enabled.Halloween3")) {
            halloween3(l, p);
          }
          break;
        case 45: 
          if (this.config.getBoolean("surprises.enabled.GiantZombie")) {
            GiantZombie(l, p);
          }
          break;
        case 46: 
          if (this.config.getBoolean("surprises.enabled.FlyingSkeleton")) {
            FlyingSkeleton(l, p);
          }
          break;
        case 47: 
          if (this.config.getBoolean("surprises.enabled.FlyingexpBottles")) {
            FlyingexpBottles(l, p);
          }
          break;
        case 48: 
          if (this.config.getBoolean("surprises.enabled.FlyingTnT")) {
            FlyingTnT(l, p);
          }
          break;
        case 49: 
          if (this.config.getBoolean("surprises.enabled.RandomMobs")) {
            RandomMobs(l, p);
          }
          break;
        case 50: 
          if (this.config.getBoolean("surprises.enabled.RandomAnimals")) {
            RandomAnimals(l, p);
          }
          break;
        case 51: 
          if (this.config.getBoolean("surprises.enabled.ObsidianCage")) {
            build1(l, p);
          }
          break;
        case 52: 
          if (this.config.getBoolean("surprises.enabled.Minerals")) {
            Minerals(l, p);
          }
          break;
        case 53: 
          if (this.config.getBoolean("surprises.enabled.Elytra")) {
            Elytra(l, p);
          }
          break;
        case 54: 
          if (this.config.getBoolean("surprises.enabled.Shield")) {
            Shield(l, p);
          }
          break;
        case 55: 
            if (this.config.getBoolean("surprises.enabled.Shulkers")) {
              Shulkers(l, p);
            }
            break;
        }
      }
    }
  }
  //}
  
  private void Shulkers(Location l, Player p) {
	  World w = p.getWorld();
	    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.RandomAnimals"));
	    for (int i = 0; i < 5; i++)
	    {
	      w.spawnEntity(l, EntityType.SHULKER);
	    }
	  }
  

  private void Shield(Location l, Player p)
  {
    World world = p.getWorld();
    
    world.dropItemNaturally(l, new ItemStack(Material.SHIELD));
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.Shield"));
  }
  
  private void Elytra(Location l, Player p)
  {
    World world = p.getWorld();
    
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.Elytra"));
    world.dropItemNaturally(l, new ItemStack(Material.ELYTRA));
  }
  
  private void Minerals(Location l, Player p)
  {
    World world = p.getWorld();
    
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.Minerals"));
    for (int i = 0; i < 5; i++)
    {
      world.dropItemNaturally(l, new ItemStack(Material.DIAMOND));
      world.dropItemNaturally(l, new ItemStack(Material.GOLD_INGOT));
      world.dropItemNaturally(l, new ItemStack(Material.IRON_INGOT));
      world.dropItemNaturally(l, new ItemStack(Material.REDSTONE));
      world.dropItemNaturally(l, new ItemStack(Material.EMERALD));
      world.dropItemNaturally(l, new ItemStack(Material.COAL));
    }
  }
  
  public void build1(Location l, Player p)
  {
    World w = p.getWorld();
    Location loc = p.getLocation();
    int x = loc.getBlockX();
    int y = loc.getBlockY();
    int z = loc.getBlockZ();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.ObsidianCage"));
    
    new Location(w, -4 + x, -7 + y, -2 + z).getBlock().setType(Material.OBSIDIAN);
    new Location(w, -3 + x, -7 + y, -2 + z).getBlock().setType(Material.OBSIDIAN);
    new Location(w, -2 + x, -7 + y, -2 + z).getBlock().setType(Material.OBSIDIAN);
    new Location(w, -4 + x, -6 + y, -2 + z).getBlock().setType(Material.OBSIDIAN);
    new Location(w, -3 + x, -6 + y, -2 + z).getBlock().setType(Material.GLASS);
    new Location(w, -2 + x, -6 + y, -2 + z).getBlock().setType(Material.OBSIDIAN);
    new Location(w, -4 + x, -5 + y, -2 + z).getBlock().setType(Material.OBSIDIAN);
    new Location(w, -3 + x, -5 + y, -2 + z).getBlock().setType(Material.OBSIDIAN);
    new Location(w, -2 + x, -5 + y, -2 + z).getBlock().setType(Material.OBSIDIAN);
    new Location(w, -4 + x, -7 + y, -1 + z).getBlock().setType(Material.OBSIDIAN);
    new Location(w, -3 + x, -7 + y, -1 + z).getBlock().setType(Material.STATIONARY_WATER);
    new Location(w, -2 + x, -7 + y, -1 + z).getBlock().setType(Material.OBSIDIAN);
    new Location(w, -4 + x, -6 + y, -1 + z).getBlock().setType(Material.GLASS);
    new Location(w, -3 + x, -6 + y, -1 + z).getBlock().setType(Material.STATIONARY_WATER);
    new Location(w, -2 + x, -6 + y, -1 + z).getBlock().setType(Material.GLASS);
    new Location(w, -4 + x, -5 + y, -1 + z).getBlock().setType(Material.OBSIDIAN);
    new Location(w, -3 + x, -5 + y, -1 + z).getBlock().setType(Material.OBSIDIAN);
    new Location(w, -2 + x, -5 + y, -1 + z).getBlock().setType(Material.OBSIDIAN);
    new Location(w, -4 + x, -7 + y, 0 + z).getBlock().setType(Material.OBSIDIAN);
    new Location(w, -3 + x, -7 + y, 0 + z).getBlock().setType(Material.OBSIDIAN);
    new Location(w, -2 + x, -7 + y, 0 + z).getBlock().setType(Material.OBSIDIAN);
    new Location(w, -4 + x, -6 + y, 0 + z).getBlock().setType(Material.OBSIDIAN);
    new Location(w, -3 + x, -6 + y, 0 + z).getBlock().setType(Material.GLASS);
    new Location(w, -2 + x, -6 + y, 0 + z).getBlock().setType(Material.OBSIDIAN);
    new Location(w, -4 + x, -5 + y, 0 + z).getBlock().setType(Material.OBSIDIAN);
    new Location(w, -3 + x, -5 + y, 0 + z).getBlock().setType(Material.OBSIDIAN);
    new Location(w, -2 + x, -5 + y, 0 + z).getBlock().setType(Material.OBSIDIAN);
  }
  
  private void RandomAnimals(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.RandomAnimals"));
    for (int i = 0; i < 5; i++)
    {
      w.spawnEntity(l, EntityType.CHICKEN);
      w.spawnEntity(l, EntityType.COW);
      w.spawnEntity(l, EntityType.PIG);
      w.spawnEntity(l, EntityType.RABBIT);
      w.spawnEntity(l, EntityType.WOLF);
    }
  }
  
  private void RandomMobs(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.RandomMobs"));
    for (int i = 0; i < 5; i++)
    {
      w.spawnEntity(l, EntityType.ZOMBIE);
      w.spawnEntity(l, EntityType.SPIDER);
      w.spawnEntity(l, EntityType.SKELETON);
      w.spawnEntity(l, EntityType.CREEPER);
      w.spawnEntity(l, EntityType.SLIME);
    }
  }
  
  private void creepers(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.creepers"));
    for (int i = 0; i < 10; i++) {
      w.spawnEntity(l, EntityType.CREEPER);
    }
  }
  
  private void Redstone(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.Redstone"));
    for (int i = 0; i < 5; i++)
    {
      w.dropItemNaturally(l, new ItemStack(Material.REDSTONE_BLOCK));
      w.dropItemNaturally(l, new ItemStack(Material.REDSTONE_COMPARATOR));
      w.dropItemNaturally(l, new ItemStack(Material.REDSTONE_LAMP_OFF));
      w.dropItemNaturally(l, new ItemStack(Material.REDSTONE_ORE));
      w.dropItemNaturally(l, new ItemStack(Material.REDSTONE_TORCH_OFF));
      w.dropItemNaturally(l, new ItemStack(Material.REDSTONE_WIRE));
      w.dropItemNaturally(l, new ItemStack(Material.TNT));
      w.dropItemNaturally(l, new ItemStack(Material.LEVER));
      w.dropItemNaturally(l, new ItemStack(Material.STONE_BUTTON));
      w.dropItemNaturally(l, new ItemStack(Material.WOOD_BUTTON));
    }
  }
  
  private void FlyingTnT(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.FlyingTnT"));
    for (int i = 0; i < 10; i++)
    {
      Entity TNT = w.spawnEntity(l, EntityType.PRIMED_TNT);
      LivingEntity bat = (LivingEntity)w.spawnEntity(l, EntityType.BAT);
      bat.setPassenger(TNT);
      bat.setHealth(1.0D);
      bat.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 10000000, 2));
    }
  }
  
  private void FlyingexpBottles(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.FlyingexpBottles"));
    for (int i = 0; i < 5; i++)
    {
      Entity exp = w.spawnEntity(l, EntityType.THROWN_EXP_BOTTLE);
      LivingEntity bat = (LivingEntity)w.spawnEntity(l, EntityType.BAT);
      bat.setPassenger(exp);
      bat.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 10000000, 2));
    }
  }
  
  private void FlyingSkeleton(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.FlyingSkeletons"));
    for (int i = 0; i < 5; i++)
    {
      Entity skeleton = w.spawnEntity(l, EntityType.SKELETON);
      LivingEntity bat = (LivingEntity)w.spawnEntity(l, EntityType.BAT);
      bat.setPassenger(skeleton);
      bat.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 10000000, 2));
    }
  }
  
  private void GiantZombie(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.GiantZombie"));
    w.spawnEntity(l, EntityType.GIANT);
  }
  
  private void halloween3(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.Halloween"));
    
    p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 5, 1));
    p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 5, 1));
    p.playSound(l, Sound.ENTITY_GHAST_DEATH, 100.0F, 0.0F);
    w.strikeLightning(p.getLocation());
  }
  
  private void halloween2(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.Halloween"));
    
    p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 5, 1));
    p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 5, 1));
    p.playSound(l, Sound.ENTITY_GHAST_SCREAM, 100.0F, 0.0F);
    w.strikeLightning(p.getLocation());
  }
  
  private void halloween1(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.Halloween"));
    
    p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 5, 1));
    p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 5, 1));
    p.playSound(l, Sound.ENTITY_BLAZE_DEATH, 100.0F, 0.0F);
    w.strikeLightning(p.getLocation());
  }
  
  private void PumpKins(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.pumpKins"));
    for (int i = 0; i < 10; i++) {
      w.dropItemNaturally(l, new ItemStack(Material.PUMPKIN));
    }
  }
  
  private void JackOLanterns(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.JackOLantern"));
    for (int i = 0; i < 10; i++) {
      w.dropItemNaturally(l, new ItemStack(Material.JACK_O_LANTERN));
    }
  }
  
  private void Zombie(Location l, Player p)
  {
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.Zombie"));
    World world = p.getWorld();
    for (int i = 0; i < 20; i++) {
      world.spawnEntity(l, EntityType.ZOMBIE);
    }
  }
  
  private void ghast(Location l, Player p)
  {
    World world = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.ghast"));
    
    world.spawnEntity(l, EntityType.GHAST);
  }
  
  private void lightningcreeper(Location l, Player p)
  {
    World world = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.lightningCreeper"));
    
    world.spawnEntity(l, EntityType.CREEPER);
    world.strikeLightning(l);
  }
  
  private void witch(Location l, Player p)
  {
    World world = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.Witch"));
    
    world.spawnEntity(l, EntityType.WITCH);
    for (int i = 0; i < 30; i++) {
      world.spawnEntity(l, EntityType.BAT);
    }
  }
  
  private void wither(Location l, Player p)
  {
    World world = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.Wither"));
    
    world.spawnEntity(l, EntityType.WITHER);
  }
  
  private void explosion3(Location l, Player p)
  {
    World world = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.explosion"));
    
    world.createExplosion(l, 10.0F);
  }
  
  private void explosion2(Location l, Player p)
  {
    World world = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.explosion"));
    
    world.createExplosion(l, 15.0F);
  }
  
  public void luckyTools(int i, Location l, Player p)
  {
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + " You have many luck! LuckyTools for you!");
  }
  
  public void explosion(Location l, Player p)
  {
    World world = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.explosion"));
    
    world.createExplosion(l, 6.0F);
  }
  
  public void woodTools(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.woodTools"));
    
    w.dropItemNaturally(l, new ItemStack(Material.WOOD_SWORD));
    w.dropItemNaturally(l, new ItemStack(Material.WOOD_PICKAXE));
    w.dropItemNaturally(l, new ItemStack(Material.WOOD_SPADE));
    w.dropItemNaturally(l, new ItemStack(Material.WOOD_HOE));
    w.dropItemNaturally(l, new ItemStack(Material.WOOD_AXE));
  }
  
  public void stoneTools(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.stoneTools"));
    
    w.dropItemNaturally(l, new ItemStack(Material.STONE_SWORD));
    w.dropItemNaturally(l, new ItemStack(Material.STONE_PICKAXE));
    w.dropItemNaturally(l, new ItemStack(Material.STONE_SPADE));
    w.dropItemNaturally(l, new ItemStack(Material.STONE_HOE));
    w.dropItemNaturally(l, new ItemStack(Material.STONE_AXE));
  }
  
  public void goldTools(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.goldTools"));
    
    w.dropItemNaturally(l, new ItemStack(Material.GOLD_SWORD));
    w.dropItemNaturally(l, new ItemStack(Material.GOLD_PICKAXE));
    w.dropItemNaturally(l, new ItemStack(Material.GOLD_SPADE));
    w.dropItemNaturally(l, new ItemStack(Material.GOLD_HOE));
    w.dropItemNaturally(l, new ItemStack(Material.GOLD_AXE));
  }
  
  public void leatherArmor(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.leatherArmor"));
    
    w.dropItemNaturally(l, new ItemStack(Material.LEATHER_BOOTS));
    w.dropItemNaturally(l, new ItemStack(Material.LEATHER_LEGGINGS));
    w.dropItemNaturally(l, new ItemStack(Material.LEATHER_CHESTPLATE));
    w.dropItemNaturally(l, new ItemStack(Material.LEATHER_HELMET));
  }
  
  public void goldArmor(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.goldArmor"));
    
    w.dropItemNaturally(l, new ItemStack(Material.GOLD_BOOTS));
    w.dropItemNaturally(l, new ItemStack(Material.GOLD_LEGGINGS));
    w.dropItemNaturally(l, new ItemStack(Material.GOLD_CHESTPLATE));
    w.dropItemNaturally(l, new ItemStack(Material.GOLD_HELMET));
  }
  
  public void ironTools(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.ironTools"));
    
    w.dropItemNaturally(l, new ItemStack(Material.IRON_AXE));
    w.dropItemNaturally(l, new ItemStack(Material.IRON_HOE));
    w.dropItemNaturally(l, new ItemStack(Material.IRON_SWORD));
    w.dropItemNaturally(l, new ItemStack(Material.IRON_PICKAXE));
    w.dropItemNaturally(l, new ItemStack(Material.IRON_SPADE));
  }
  
  public void diamondTools(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.diamondTools"));
    ArrayList<ItemStack> arrayList = new ArrayList<ItemStack>();
    List<ItemStack> list = arrayList;
    
    list.add(new ItemStack(Material.DIAMOND_AXE));
    list.add(new ItemStack(Material.DIAMOND_PICKAXE));
    list.add(new ItemStack(Material.DIAMOND_HOE));
    list.add(new ItemStack(Material.DIAMOND_SPADE));
    list.add(new ItemStack(Material.DIAMOND_SWORD));
    
    Collections.shuffle(list);
    
    int rand = 1 + (int)(Math.random() * 2.0D);
    for (int i = 1; i < 1 + rand; i++) {
      w.dropItemNaturally(l, (ItemStack)list.get(i));
    }
  }
  
  public void ironArmor(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.ironArmor"));
    List<ItemStack> list = new ArrayList<ItemStack>();
    
    list.add(new ItemStack(Material.IRON_BOOTS));
    list.add(new ItemStack(Material.IRON_LEGGINGS));
    list.add(new ItemStack(Material.IRON_CHESTPLATE));
    list.add(new ItemStack(Material.IRON_HELMET));
    
    Collections.shuffle(list);
    
    int rand = 2 + (int)(Math.random() * 2.0D);
    for (int i = 1; i < 1 + rand; i++) {
      w.dropItemNaturally(l, (ItemStack)list.get(i));
    }
  }
  
  public void diamondArmor(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.diamondArmor"));
    List<ItemStack> list = new ArrayList<ItemStack>();
    
    list.add(new ItemStack(Material.DIAMOND_BOOTS));
    list.add(new ItemStack(Material.DIAMOND_LEGGINGS));
    list.add(new ItemStack(Material.DIAMOND_CHESTPLATE));
    list.add(new ItemStack(Material.DIAMOND_HELMET));
    
    Collections.shuffle(list);
    
    int rand = 1 + (int)(Math.random() * 2.0D);
    for (int i = 1; i < 1 + rand; i++) {
      w.dropItemNaturally(l, (ItemStack)list.get(i));
    }
  }
  
  public void horseArmor(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.horseArmor"));
    
    w.dropItemNaturally(l, new ItemStack(Material.IRON_BARDING));
    w.dropItemNaturally(l, new ItemStack(Material.GOLD_BARDING));
    w.dropItemNaturally(l, new ItemStack(Material.DIAMOND_BARDING));
  }
  
  public void bowAndArrows(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + " Shoot your enemies down!");
    w.dropItemNaturally(l, new ItemStack(Material.BOW));
    
    int rand = 16 + (int)(Math.random() * 17.0D);
    for (int i = 16; i < 1 + rand; i++) {
      w.dropItemNaturally(l, new ItemStack(Material.ARROW));
    }
  }
  
  public void booksAndBookshelves(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.booksAndBookshelves"));
    
    int rand = 8 + (int)(Math.random() * 9.0D);
    for (int i = 8; i < 1 + rand; i++) {
      w.dropItemNaturally(l, new ItemStack(Material.BOOK));
    }
    Math.random();
    for (int i = 0; i < 1 + rand; i++) {
      w.dropItemNaturally(l, new ItemStack(Material.BOOKSHELF));
    }
  }
  
  public void vegetables(Location l, Player p)
  {
    int rand = 8 + (int)(Math.random() * 9.0D);
    for (int i = 8; i < 1 + rand; i++)
    {
      World w = p.getWorld();
      p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.vegetables"));
      int howManyPossibleHappenstances = 6;
      
      int randomChoice = 1 + (int)(Math.random() * howManyPossibleHappenstances);
      switch (randomChoice)
      {
      case 1: 
        w.dropItemNaturally(l, new ItemStack(Material.POISONOUS_POTATO)); break;
      case 2: 
        w.dropItemNaturally(l, new ItemStack(Material.POTATO)); break;
      case 3: 
      case 4: 
        w.dropItemNaturally(l, new ItemStack(Material.BAKED_POTATO)); break;
      case 5: 
      case 6: 
        w.dropItemNaturally(l, new ItemStack(Material.CARROT));
      }
    }
  }
  
  public void meats(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.meats"));
    
    int rand = 8 + (int)(Math.random() * 9.0D);
    for (int i = 8; i < 1 + rand; i++)
    {
      int howManyPossibleHappenstances = 16;
      
      int randomChoice = 1 + (int)(Math.random() * howManyPossibleHappenstances);
      switch (randomChoice)
      {
      case 1: 
      case 2: 
        w.dropItemNaturally(l, new ItemStack(Material.COOKED_CHICKEN)); break;
      case 3: 
      case 4: 
        w.dropItemNaturally(l, new ItemStack(Material.RAW_CHICKEN)); break;
      case 5: 
      case 6: 
        w.dropItemNaturally(l, new ItemStack(Material.COOKED_FISH)); break;
      case 7: 
      case 8: 
        w.dropItemNaturally(l, new ItemStack(Material.RAW_FISH)); break;
      case 9: 
      case 10: 
        w.dropItemNaturally(l, new ItemStack(Material.COOKED_BEEF)); break;
      case 11: 
      case 12: 
        w.dropItemNaturally(l, new ItemStack(Material.RAW_BEEF)); break;
      case 13: 
      case 14: 
        w.dropItemNaturally(l, new ItemStack(Material.PORK)); break;
      case 15: 
      case 16: 
        w.dropItemNaturally(l, new ItemStack(Material.GRILLED_PORK));
      }
    }
  }
  
  public void brewingItems(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.brewingItems"));
    int rand = 16 + (int)(Math.random() * 17.0D);
    for (int i = 16; i < 1 + rand; i++)
    {
      int howManyPossibleHappenstances = 34;
      
      int randomChoice = 1 + (int)(Math.random() * howManyPossibleHappenstances);
      switch (randomChoice)
      {
      case 1: 
        w.dropItemNaturally(l, new ItemStack(Material.ENDER_PEARL)); break;
      case 2: 
        w.dropItemNaturally(l, new ItemStack(Material.GHAST_TEAR)); break;
      case 3: 
      case 4: 
      case 5: 
        w.dropItemNaturally(l, new ItemStack(Material.NETHER_WARTS)); break;
      case 6: 
        w.dropItemNaturally(l, new ItemStack(Material.EYE_OF_ENDER)); break;
      case 7: 
      case 8: 
        w.dropItemNaturally(l, new ItemStack(Material.SPIDER_EYE)); break;
      case 9: 
        w.dropItemNaturally(l, new ItemStack(Material.FERMENTED_SPIDER_EYE)); break;
      case 10: 
      case 11: 
        w.dropItemNaturally(l, new ItemStack(Material.BLAZE_POWDER)); break;
      case 12: 
        w.dropItemNaturally(l, new ItemStack(Material.BLAZE_ROD)); break;
      case 13: 
      case 14: 
        w.dropItemNaturally(l, new ItemStack(Material.MAGMA_CREAM)); break;
      case 15: 
      case 16: 
      case 17: 
      case 18: 
        w.dropItemNaturally(l, new ItemStack(Material.REDSTONE)); break;
      case 19: 
      case 20: 
      case 21: 
      case 22: 
        w.dropItemNaturally(l, new ItemStack(Material.GLOWSTONE_DUST)); break;
      case 24: 
      case 25: 
      case 26: 
        w.dropItemNaturally(l, new ItemStack(Material.SULPHUR)); break;
      case 27: 
      case 28: 
        w.dropItemNaturally(l, new ItemStack(Material.GOLDEN_CARROT)); break;
      case 29: 
      case 30: 
      case 31: 
      case 32: 
        w.dropItemNaturally(l, new ItemStack(Material.GOLD_NUGGET)); break;
      case 33: 
      case 34: 
        w.dropItemNaturally(l, new ItemStack(Material.SPECKLED_MELON));
      }
    }
  }
  
  public void TNT(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.TNT"));
    for (int i = 1; i < 9; i++) {
      w.dropItemNaturally(l, new ItemStack(Material.TNT));
    }
  }
  
  public void obsidian(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.obsidian"));
    for (int i = 1; i < 15; i++) {
      w.dropItemNaturally(l, new ItemStack(Material.OBSIDIAN));
    }
  }
  
  public void enchantmentTable(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.enchantmentTable"));
    
    w.dropItemNaturally(l, new ItemStack(Material.ENCHANTMENT_TABLE));
    w.dropItemNaturally(l, new ItemStack(Material.LAPIS_BLOCK));
    w.dropItemNaturally(l, new ItemStack(Material.LAPIS_BLOCK));
  }
  
  public void dragonEgg(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.dragonEgg"));
    
    w.dropItemNaturally(l, new ItemStack(Material.DRAGON_EGG));
  }
  
  public void enderChests(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.enderChests"));
    for (int i = 1; i < 3; i++) {
      w.dropItemNaturally(l, new ItemStack(Material.ENDER_CHEST));
    }
  }
  
  public void beacon(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.beacon"));
    
    w.dropItemNaturally(l, new ItemStack(Material.BEACON));
  }
  
  public void droppers(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.droppers"));
    
    w.dropItemNaturally(l, new ItemStack(Material.DROPPER, 8));
  }
  
  public void hay(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.hay"));
    w.dropItemNaturally(l, new ItemStack(Material.HAY_BLOCK, 8));
  }
  
  public void gold(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.gold"));
    
    w.dropItemNaturally(l, new ItemStack(Material.GOLD_INGOT, 10));
  }
  
  public void paintings(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.paintings"));
    
    w.dropItemNaturally(l, new ItemStack(Material.PAINTING, 4));
  }
  
  public void saddles(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.saddles"));
    
    w.dropItemNaturally(l, new ItemStack(Material.SADDLE, 2));
    w.spawnEntity(l, EntityType.HORSE);
  }
  
  public void cake(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.cake"));
    
    w.dropItemNaturally(l, new ItemStack(Material.CAKE));
  }
  
  public void rottenFlesh(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.rottenFlesh"));
    
    w.dropItemNaturally(l, new ItemStack(Material.ROTTEN_FLESH));
  }
  
  public void netherStar(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.netherStar"));
    
    w.dropItemNaturally(l, new ItemStack(Material.NETHER_STAR));
  }
  
  public void romanticRose(Location l, Player p)
  {
    World w = p.getWorld();
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.romanticRose"));
    
    w.dropItemNaturally(l, new ItemStack(Material.RED_ROSE));
  }
  
  public void mobHeads(Location l, Player p)
  {
    for (int i = 1; i < 3; i++)
    {
      World w = p.getWorld();
      p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.mobHeads"));
      switch (i)
      {
      case 1: 
        w.dropItemNaturally(l, new ItemStack(Material.SKULL)); break;
      case 2: 
        w.dropItemNaturally(l, new ItemStack(Material.SKULL)); break;
      case 3: 
        w.dropItemNaturally(l, new ItemStack(Material.SKULL)); break;
      case 4: 
        w.dropItemNaturally(l, new ItemStack(Material.SKULL)); break;
      case 5: 
        w.dropItemNaturally(l, new ItemStack(Material.SKULL));
      }
    }
  }
  
  public void enderPearls(Location l, Player p)
  {
    World w = p.getWorld();
    
    p.sendMessage(ChatColor.GOLD + "[SLB]" + ChatColor.WHITE + this.config.getString("messages.enderPearls"));
    w.dropItemNaturally(l, new ItemStack(Material.EYE_OF_ENDER, 10));
    w.dropItemNaturally(l, new ItemStack(Material.ENDER_PEARL, 16));
  }
  
  private void ConfigDefaults()
  {
    this.config.addDefault("settings.BroadcastBlockBreak", Boolean.valueOf(false));
    this.config.addDefault("settings.CraftingRecipe", Boolean.valueOf(true));
    this.config.addDefault("settings.FastBreak", Boolean.valueOf(true));
    this.config.addDefault("settings.CooldownTime", 0);
    this.config.addDefault("surprises.enabled.luckytools", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.enderPearls", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.explosion", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.wither", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.woodTools", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.Zombies", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.Witch", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.stoneTools", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.goldTools", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.lightningCreeper", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.leatherArmor", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.ghast", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.goldArmor", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.creepers", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.gold", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.ironTools", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.diamondTools", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.beacon", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.enderChests", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.diamondArmor", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.ironArmor", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.dragonEgg", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.enchantmentTable", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.horseArmor", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.obsidian", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.TNT", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.bowAndArrows", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.brewingItems", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.potionItems", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.booksandBookshelves", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.meats", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.vegatables", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.PumpKins", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.Halloween1", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.Halloween2", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.Halloween3", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.GiantZombie", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.FlyingSkeleton", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.FlyingexpBottles", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.FlyingTnT", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.RandomMobs", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.RandomAnimals", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.ObsidianCage", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.Minerals", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.Elytra", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.Shield", Boolean.valueOf(true));
    this.config.addDefault("surprises.enabled.Shulkers", Boolean.valueOf(true));
    this.config.addDefault("messages.enderPearls", "This is for you");
    this.config.addDefault("messages.explosion", "This is for you");
    this.config.addDefault("messages.wither", "This is for you");
    this.config.addDefault("messages.woodTools", "This is for you");
    this.config.addDefault("messages.Zombies", "This is for you");
    this.config.addDefault("messages.Witch", "This is for you");
    this.config.addDefault("messages.stoneTools", "This is for you");
    this.config.addDefault("messages.goldTools", "This is for you");
    this.config.addDefault("messages.lightningCreeper", "This is for you");
    this.config.addDefault("messages.leatherArmor", "This is for you");
    this.config.addDefault("messages.ghast", "This is for you");
    this.config.addDefault("messages.goldArmor", "This is for you");
    this.config.addDefault("messages.creepers", "This is for you");
    this.config.addDefault("messages.gold", "This is for you");
    this.config.addDefault("messages.ironTools", "This is for you");
    this.config.addDefault("messages.diamondTools", "This is for you");
    this.config.addDefault("messages.ironArmor", "This is for you");
    this.config.addDefault("messages.dragonEgg", "This is for you");
    this.config.addDefault("messages.enchantmentTable", "This is for you");
    this.config.addDefault("messages.horseArmor", "This is for you");
    this.config.addDefault("messages.obsidian", "This is for you");
    this.config.addDefault("messages.TNT", "This is for you");
    this.config.addDefault("messages.bowAndArrows", "This is for you");
    this.config.addDefault("messages.brewingItems", "This is for you");
    this.config.addDefault("messages.potionItems", "This is for you");
    this.config.addDefault("messages.booksandbookshelves", "This is for you");
    this.config.addDefault("messages.meats", "This is for you");
    this.config.addDefault("messages.vegatables", "This is for you");
    this.config.addDefault("messages.PumpKins", "This is for you");
    this.config.addDefault("messages.Halloween", "This is for you");
    this.config.addDefault("messages.GiantZombie", "This is for you");
    this.config.addDefault("messages.FlyingSkeleton", "This is for you");
    this.config.addDefault("messages.FlyingexpBottles", "This is for you");
    this.config.addDefault("messages.FlyingTnT", "This is for you");
    this.config.addDefault("messages.RandomMobs", "This is for you");
    this.config.addDefault("messages.ObsidianCage", "This is for you");
    this.config.addDefault("messages.Minerals", "This is for you");
    this.config.addDefault("messages.Elytra", "This is for you");
    this.config.addDefault("messages.Shield", "This is for you");
    this.config.options().copyDefaults(true);
  }
}
